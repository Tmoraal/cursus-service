package nl.kzaconnected.cursus.repository;

import nl.kzaconnected.cursus.model.Dao.Slagingscriterium;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SlagingscriteriumRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private SlagingscriteriumRepository SlagingscriteriumRepository;

    @Test
    public void findBySlagingscriteriumShouldReturnSlagingscriteriumWithId() {
        Slagingscriterium slagingscriterium = Slagingscriterium.builder().slagingscriterium("Slagingscriterium1").build();
        entityManager.persist(slagingscriterium);
        entityManager.flush();
        Slagingscriterium found = SlagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium());
        assertThat(found.getId()).isNotNull();
        assertThat(found.getSlagingscriterium()).isEqualTo(slagingscriterium.getSlagingscriterium());
    }
}