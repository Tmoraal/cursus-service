package nl.kzaconnected.cursus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursusServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursusServiceApplication.class, args);
	}
}
