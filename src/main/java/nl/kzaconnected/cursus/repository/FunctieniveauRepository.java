package nl.kzaconnected.cursus.repository;

import nl.kzaconnected.cursus.model.Dao.Functieniveau;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FunctieniveauRepository extends JpaRepository<Functieniveau,Long> {

    public Functieniveau findByFunctieniveau(String functieniveau);
}
